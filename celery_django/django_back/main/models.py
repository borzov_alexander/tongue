from datetime import datetime
from enum import Enum

from django.db import models


class IterationStatus(Enum):
    NOT_STARTED = 'NOT_STARTED'
    IN_PROGRESS = 'IN_PROGRESS'
    FINISHED = 'FINISHED'
    REVIEWED = 'REVIEWED'


class Session(models.Model):
    title = models.CharField(max_length=200)
    created_date = models.DateTimeField(default=datetime.now)


class Iteration(models.Model):
    session = models.ForeignKey(Session, on_delete=models.CASCADE)
    prev = models.ForeignKey("self", on_delete=models.SET_NULL, blank=True, null=True)
    status = models.CharField(max_length=32,
                              choices=[(tag, tag.value) for tag in IterationStatus],
                              default=IterationStatus.NOT_STARTED)
    created_date = models.DateTimeField(default=datetime.now)
    input_value = models.OneToOneField('Sentence', on_delete=models.SET_NULL, primary_key=False,
                                       related_name='input_for',
                                       blank=True, null=True)


class Group(models.Model):
    iteration = models.ForeignKey(Iteration, on_delete=models.CASCADE)
    suitable = models.BooleanField(default=None, blank=True, null=True)


class Sentence(models.Model):
    count = models.IntegerField(default=0)
    group = models.ForeignKey(Group, on_delete=models.CASCADE, null=True, blank=True)
    text = models.TextField()
    important_words = models.TextField()
    tree = models.BinaryField(null=True, blank=True)
