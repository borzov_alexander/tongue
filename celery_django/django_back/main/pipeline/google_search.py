import urllib
import urllib.request
from urllib.parse import urlparse

import requests
from bs4 import BeautifulSoup

from django_back.constants import SITE_FOR_SEARCH, OPERATION


def generate_query(words: str):
    if SITE_FOR_SEARCH is not None and SITE_FOR_SEARCH != "":
        site = "site%3A" + SITE_FOR_SEARCH + " "
    else:
        site = ""
    words_list = words.split(" ")
    query = site
    for word in words_list:
        query = query + '"' + word + '"' + " "  # "%2B" - символ плюса
    return query

def generate_query_for_1_word(word: str):
    assert len(word.split(" ")) == 1, str(word.split(" "))
    if SITE_FOR_SEARCH is not None and SITE_FOR_SEARCH != "":
        site = "site%3A" + SITE_FOR_SEARCH + " "
    else:
        site = ""
    return site + '"' + word + '" '  # "%2B" - символ плюса


def generate_query_for_2_words(words: str):
    assert len(words.split(" ")) == 2, str(words.split(" "))
    if SITE_FOR_SEARCH is not None and SITE_FOR_SEARCH != "":
        site = "site%3A" + SITE_FOR_SEARCH + " "
    else:
        site = ""
    # query = site + '"' + words[0] + OPERATION + words[1] + '"' + "OR" + '"' + words[1] + OPERATION + words[0] + '"'
    query = site + words[0] + OPERATION + words[1] + "OR" + words[1] + OPERATION + words[0]
    return query

def get_links_with_google_parsing(words: str, offset: int = 0, count: int = 10):
    LINKS_ON_PAGE = 10  # CONST
    links = list()
    count_words = len(words.split(" "))
    if count_words == 2:
        query = generate_query_for_2_words(words)
    elif count_words == 1:
        query = generate_query_for_1_word(words)
    else:
        assert False, 'Неверное количество слов для запроса'
    for i in range(int(count / LINKS_ON_PAGE)):
        links_from_page = _get_links_from_google_page_search(query, offset=offset + i * LINKS_ON_PAGE)
        links.extend(links_from_page)

    links_from_page = _get_links_from_google_page_search(query, offset=offset + int(count / 10) * 10)
    links.extend(links_from_page[0:count % 10])

    return links


# для тестов
# def get_links_from_google_page_search(word: str, offset: int = 0):
#     links = list()
#     for i in range(offset,offset+10):
#         links.append(i)

def _get_links_from_google_page_search(query: str, offset: int = 0):
    N_LINKS_TO_GOOGLE_HELP = 2  # CONST
    url = 'https://google.com/search?q=' + query + "&start=" + str(offset)
    requests.adapters.DEFAULT_RETRIES = 1
    response = requests.get(url, timeout=(1, 3))
    soup = BeautifulSoup(response.text, 'html.parser')
    links = list()
    for tag in soup.find_all(href=True):
        link_raw: str = tag['href']
        link = _parse_raw_link(link_raw)
        if link is not None:
            links.append(link)
    links = links[0:len(links) - N_LINKS_TO_GOOGLE_HELP]
    return links


def _parse_raw_link(link_raw: str):
    link_prefix = "/url?q="
    if link_raw[0:len(link_prefix)] == link_prefix:
        link_with_ved = link_raw[7:len(link_raw)]
        link = link_with_ved[0:link_with_ved.find("&")]
        url = urllib.parse.unquote(link)
        return url
    return None
