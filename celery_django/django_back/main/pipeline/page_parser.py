import requests
from bs4 import BeautifulSoup
from bs4.element import Comment


def tag_visible(element):
    if element.parent.name in ['style', 'script', 'head', 'title', 'meta', '[document]']:
        return False
    if element.parent.name in ['h1', 'h2', 'h3']:
        return False
    elif element.parent.parent is not None:
        if element.parent.parent.name in ['h1', 'h2', 'h3']:
            return False
        if element.parent.parent.parent is not None and element.parent.parent.parent.name in ['h1', 'h2', 'h3']:
            return False
    if isinstance(element, Comment):
        return False
    return True

#TODO: нужно сделать чтобы в зависимости от тега происходили разные действия.
# Например, если это перечисление, то можно после каждого пункта поставить точку чтобы потом это на разные предложения разделилось
#  в некоторые теги можно вообще не заходить
def get_texts_from_page_by_link(url: str) -> list:
    requests.adapters.DEFAULT_RETRIES = 1
    res = requests.get(url, timeout=(1, 3))
    html_page = res.content
    soup = BeautifulSoup(html_page, 'html.parser')
    # text = soup.find_all(text=True)

    # for script in soup(["script", "style"]):
    #     script.extract()

    # text = soup.get_text()
    texts = soup.find_all(text=True) #soup.findAll(text=True)

    visible_texts = filter(tag_visible, texts)
    lines = [line.strip() for line in visible_texts]
    lines = [line for line in lines if len(line) != 0]
    # chunks: list = [phrase.strip() for line in lines for phrase in line.split("  ")]

    # return chunks
    text = ' '.join(line for line in lines if line)
    return [text]
