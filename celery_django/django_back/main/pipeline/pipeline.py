import itertools
import multiprocessing
from typing import List

from treelib import Tree

from django_back.constants import COUNT_TREADS_FOR_URL_LOAD, COUNT_LINKS
from main.pipeline.google_search import get_links_with_google_parsing
from main.pipeline.page_parser import get_texts_from_page_by_link
from main.pipeline.text_analyzer import TextAnalyzer, SentenceInfo
import sys
import time
import billiard as multiprocessing


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def get_sentences_with_search_words_in_link(link: str, text_analyzer, words: str) -> List[str]:
    sentences_with_search_words: List[str] = []
    try:
        # st = time.time()

        texts = get_texts_from_page_by_link(link)

        # et = time.time()
        # elapsed_time = et - st

        # print('get text from link time:', elapsed_time, 'seconds')
        sentences = text_analyzer.to_sentences_list(texts)
        sentences_with_word_in_link = text_analyzer.find_sentences_with_words(words, sentences)
        sentences_with_search_words.extend(sentences_with_word_in_link)
    except Exception as err:
        eprint(err)
    return sentences_with_search_words


def find_sentences_with_search_words(text_analyzer, words: str):
    links = get_links_with_google_parsing(words, offset=0, count=COUNT_LINKS)



    pool = multiprocessing.Pool(COUNT_TREADS_FOR_URL_LOAD)
    func_args = zip(links, itertools.repeat(text_analyzer), itertools.repeat(words))
    list_of_lists: List = pool.starmap(get_sentences_with_search_words_in_link, func_args)
    flat_list = [item for sublist in list_of_lists for item in sublist]


    return flat_list


def group__sentence_infos__by_word(text_analyzer: TextAnalyzer, sentence_infos: List[SentenceInfo]) -> \
        List[List[SentenceInfo]]:
    groups_set = dict()
    for sentInfo in sentence_infos:
        important_word = text_analyzer.lemmatize(sentInfo.important_words)[0]
        if groups_set.get(important_word, None) is None:
            groups_set[important_word] = list()

        groups_set.get(important_word).append(sentInfo)

    return list(groups_set.values())


def group__sentence_infos__by_tree(text_analyzer: TextAnalyzer, sentence_infos: List[SentenceInfo]) -> \
        List[List[SentenceInfo]]:
    # TODO: Надо сделать группировку. Пока что никак не группируем
    return [[info] for info in sentence_infos]


def make_groups(tree: Tree, words: str) -> List[List[SentenceInfo]]:
    text_analyzer = TextAnalyzer()

    sentences_with_search_words = find_sentences_with_search_words(text_analyzer, words)

    if tree is not None:
        st = time.time()

        sentence_infos: List[SentenceInfo] = text_analyzer.get__sentence_info__with_same_tree(tree,
                                                                                              sentences_with_search_words)

        et = time.time()
        elapsed_time = et - st
        print('making sentence_infos time:', elapsed_time, 'seconds')
        groups: List[List[SentenceInfo]] = group__sentence_infos__by_word(text_analyzer, sentence_infos)
    else:
        st = time.time()
        word = words
        sentence_infos: List[SentenceInfo] = text_analyzer.get__sentence_info__with_main_word(word,
                                                                                              sentences_with_search_words)

        et = time.time()
        elapsed_time = et - st
        print('making sentence_infos time:', elapsed_time, 'seconds')
        groups: List[List[SentenceInfo]] = group__sentence_infos__by_tree(text_analyzer, sentence_infos)

    return groups
