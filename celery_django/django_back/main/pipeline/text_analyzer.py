from dataclasses import dataclass
from typing import List

import pymorphy2
import spacy
from treelib import Tree


@dataclass
class SentenceInfo:
    sentence: str
    important_words: str
    tree: Tree | None


class TextAnalyzer:
    def __init__(self):
        self.morph = pymorphy2.MorphAnalyzer()

        try:
            self.nlp = spacy.load("ru_core_news_sm")
        except Exception:
            spacy.cli.download("ru_core_news_sm")
            self.nlp = spacy.load("ru_core_news_sm")

        self.nlp.add_pipe('sentencizer')

    def lemmatize(self, text):
        words = text.split()
        res = list()
        for word in words:
            p = self.morph.parse(word)[0]
            res.append(p.normal_form)

        return res

    def to_sentences_list(self, texts: list):
        MAX_SENTENCE_LEN = 500
        sentences = list()
        for text in texts:
            doc = self.nlp(text)
            for sent in doc.sents:
                sent_striped = sent.text.strip()
                if len(sent_striped) < MAX_SENTENCE_LEN:
                    sentences.append(sent_striped)
        return sentences

    def find_sentences_with_words(self, words: str, sentences: list):
        sentences_with_word = []
        for s in sentences:
            lemmatized_words = self.lemmatize(words)
            lemmatized_s = self.lemmatize(s)
            if len(lemmatized_s) > 1 and all(word in lemmatized_s for word in lemmatized_words):
                sentences_with_word.append(s)
        return sentences_with_word

    def find_nodes_with_same_text(self, doc, root, text, acc: list):
        if self.lemmatize(root.text) == text:
            acc.append((root, doc))

        for child in root.children:
            self.find_nodes_with_same_text(doc, child, text, acc)
        return

    def find_main_word_with_related_words(self, root, word1: str, word2: str) -> str | None:
        is_word1_contains = False
        is_word2_contains = False

        for child in root.children:
            if self.lemmatize(child.text) == self.lemmatize(word1):
                is_word1_contains = True
            if self.lemmatize(child.text) == self.lemmatize(word2):
                is_word2_contains = True

        if is_word1_contains and is_word2_contains:
            return root.text

        for child in root.children:
            word = self.find_main_word_with_related_words(child, word1, word2)
            if word is not None:
                return word
        return None

    def get__sentence_info__with_same_tree(self, tree: Tree, sentences_with_search_words) -> List[SentenceInfo]:
        new_sentence_infos: List[SentenceInfo] = list()
        for sentence in sentences_with_search_words:
            doc = self.nlp(sentence)
            root = next(doc.sents).root

            print(tree.show())
            children = tree.children(tree.root)
            word1 = children[0].tag
            word2 = children[1].tag
            result = self.find_main_word_with_related_words(root, word1, word2)
            if result is not None:
                new_sentence_infos.append(SentenceInfo(sentence, result, None))

        return new_sentence_infos

    def get__sentence_info__with_main_word(self, word, sentences_with_search_words) -> List[SentenceInfo]:
        new_sentence_infos: List[SentenceInfo] = list()
        for sentence in sentences_with_search_words:
            tree = next(self.nlp(sentence).sents).root
            acc = list()
            self.find_nodes_with_same_text(None, tree, self.lemmatize(word), acc)
            for root_tuple in acc:
                root = root_tuple[0]
                size = sum(1 for _ in root.children)
                for i in range(size):
                    child1 = self.get_number_n(root.children, i)
                    if child1.dep_ == 'punct':
                        break
                    for j in range(i + 1, size):
                        child2 = self.get_number_n(root.children, j)
                        if child2.dep_ == 'punct':
                            break

                        new_tree = self.make_tree_with_related_nodes(root, child1, child2)
                        new_sentence_infos.append(
                            SentenceInfo(sentence, child1.text + " " + child2.text, new_tree))
        return new_sentence_infos

    @staticmethod
    def get_number_n(generator, n):
        for i, v in enumerate(generator):
            if i == n:
                return v
        return None

    @staticmethod
    def make_tree_with_related_nodes(root, child1, child2) -> Tree:
        new_tree = Tree()
        new_root = new_tree.create_node(root.text, data={'dep_': root.dep_})
        new_tree.create_node(child1.text, parent=new_root.identifier, data={'dep_': child1.dep_})
        new_tree.create_node(child2.text, parent=new_root.identifier, data={'dep_': child2.dep_})
        return new_tree


    # ***************************** Всё это оказалось не нужно для примитивной реализации *****************************
    # def _make_tree_with_related_words_recur(self, new_tree: Tree, father: str | None, src_tree, word1, word2):
    #     # my_node = None
    #     if father is None:
    #         my_node = new_tree.create_node(src_tree.text, data={'dep_': src_tree.dep_})
    #     else:
    #         my_node = new_tree.create_node(src_tree.text, parent=father, data={'dep_': src_tree.dep_})
    #
    #     # if src_tree.text == word1 or src_tree.text == word2:  # FIXME: ОШИБКА при такой реализации если в предложении два раза одно слово.
    #     #     return True
    #
    #     # FIXME: нужно сделать чтобы оба проверялись
    #     is_word1_found = False
    #     is_word2_found = False
    #     for src_tree_child in src_tree.children:
    #         if src_tree_child.text == word1 and not is_word1_found:
    #             is_word1_found = True
    #             new_tree.create_node(src_tree_child.text, data={'dep_': src_tree_child.dep_}, parent=my_node.identifier)
    #
    #         if src_tree_child.text == word2 and not is_word2_found:
    #             is_word2_found = True
    #             new_tree.create_node(src_tree_child.text, data={'dep_': src_tree_child.dep_}, parent=my_node.identifier)
    #         # self._make_tree_with_related_words_recur(new_tree, my_node.identifier, src_tree_child, word1, word2)
    #         # new_tree.remove_node(my_node.identifier)
    #         return False
    #     return True
    #
    #
    # def make_tree_with_related_words(self, root, word1, word2) -> Tree:
    #     tree = Tree()
    #     self._make_tree_with_related_words_recur(tree, None, root, word1, word2)
    #     return tree
    #
    # def find(self, template, docs: list) -> list:
    #     to_return = []
    #     start_nodes = []
    #     for doc in docs:
    #         self.find_nodes_with_same_text(doc, next(doc.sents).root, self.lemmatize(template.text), start_nodes)
    #
    #     for root_doc in start_nodes:
    #         v_tree = Tree()
    #
    #         if self.is_contains_subtree(root_doc[0], template, v_tree, None):
    #             to_return.append((root_doc[1], v_tree))
    #     return to_return
    #     def is_contains_subtree(self, tree, subtree, v_tree: Tree, father: str | None) -> bool:
    #         # my_node = None
    #         if father is None:
    #             my_node = v_tree.create_node(tree.text, data={'dep_': tree.dep_})
    #         else:
    #             my_node = v_tree.create_node(tree.text, parent=father, data={'dep_': tree.dep_})
    #         if subtree.dep_ != 'ROOT' and tree.dep_ != subtree.dep_:
    #             v_tree.remove_node(my_node.identifier)
    #             return False
    #
    #         for subtree_child in subtree.children:
    #             if not any(self.is_contains_subtree(tree_child, subtree_child, v_tree, my_node.identifier) for tree_child in
    #                        tree.children):
    #                 v_tree.remove_node(my_node.identifier)
    #                 return False
    #         return True