from typing import List

from main import tasks
from main.models import Iteration, IterationStatus, Sentence, Group, Session


def review_iteration(session_id: int, iteration_id: int, group_ids_list: List[int]):
    prev_iteration: Iteration = Iteration.objects.get(pk=iteration_id)
    prev_iteration.status = IterationStatus.REVIEWED
    Iteration.save(prev_iteration)

    for group_id in group_ids_list:
        group: Group = Group.objects.get(pk=group_id)
        group.suitable = True
        Group.save(group)

        sentence: Sentence = group.sentence_set.order_by('-count').first()
        create_iteration_and_start_task(sentence, session_id, prev_iteration)


def clear_db():
    Session.objects.all().delete()
    Sentence.objects.all().delete()


def create_iteration_and_start_task(sentence: Sentence, session_id: int,
                                    prev_iteration: Iteration | None = None) -> Iteration:
    iteration = Iteration.objects.create(session_id=session_id, prev=prev_iteration, input_value=sentence)
    Iteration.save(iteration)
    tasks.run_iteration.delay(iteration.id)
    return iteration


def get_sessions() -> List[Session]:
    return Session.objects.order_by('-created_date').all()


def start_new_session(sentence_text) -> Session:
    sentence = Sentence.objects.create(text=sentence_text, important_words=sentence_text)
    session = Session.objects.create(title=sentence_text)
    create_iteration_and_start_task(sentence, session.id)
    return session
