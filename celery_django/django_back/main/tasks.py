import pickle
from typing import List

from celery import shared_task

from .models import Sentence, Iteration, IterationStatus, Group
from .pipeline.pipeline import make_groups
from .pipeline.text_analyzer import SentenceInfo


@shared_task
def run_iteration(iteration_id: int) -> int:
    iteration = Iteration.objects.get(pk=iteration_id)
    iteration.status = IterationStatus.IN_PROGRESS
    Iteration.save(iteration)
    tree_in_db = iteration.input_value.tree

    tree_obj = None
    if tree_in_db is not None:
        tree_obj = pickle.loads(iteration.input_value.tree)

    groups: List[List[SentenceInfo]] = make_groups(tree_obj, iteration.input_value.important_words)
    for group in groups:
        group_in_db = Group.objects.create(iteration=iteration)
        for sentence_info in group:
            tree_bytes = None
            if sentence_info.tree is not None:
                tree_bytes = pickle.dumps(sentence_info.tree)
            Sentence.objects.create(group=group_in_db,
                                    count=len(group),
                                    important_words=sentence_info.important_words,
                                    tree=tree_bytes,
                                    text=sentence_info.sentence,
                                    )
    iteration.status = IterationStatus.FINISHED
    Iteration.save(iteration)
    return iteration.id
