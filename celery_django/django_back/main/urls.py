from django.urls import path

from . import views

app_name = 'djangoServer'
urlpatterns = [
    path('', views.session_list, name='session_list'),
    path('create_session_form', views.create_session_form, name='create_session_form'),
    path('create_session', views.submit_create_session, name='submit_create_session'),
    path('session/<int:session_id>', views.session_view, name='session-view'),
    path('iteration/<int:iteration_id>', views.iteration_view, name='iteration_view'),
    path('review_iteration/<int:session_id>/<int:iteration_id>', views.submit_iteration_review, name='submit_iteration_review'),
    path('clear', views.clear, name='clear'),
]
