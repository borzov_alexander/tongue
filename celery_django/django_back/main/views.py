from typing import List

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404

from main.models import Session, Iteration, IterationStatus
from main.service import session_service


# TODO: transactional
def session_list(request):
    sessions: List[Session] = session_service.get_sessions()

    session_list_view = [{'id': session.id, 'title': session.title} for session in list(sessions)]
    context = {
        'sessions_list': session_list_view,
    }
    return render(request, 'session_list.html', context=context)


def create_session_form(request):
    return render(request, 'start_session.html')


# TODO: transactional
def submit_create_session(request):
    sentence_text = request.POST['sentence']

    session = session_service.start_new_session(sentence_text)
    return HttpResponseRedirect("/session/" + str(session.id))


# TODO: transactional
def session_view(request, session_id):
    session = get_object_or_404(Session, pk=session_id)
    iterations = Iteration.objects.all().filter(session=session).order_by('-created_date').all()

    reviewed_list = []
    not_reviewed_list = []
    for iteration in iterations:
        obj = {
            'text': iteration.input_value.text,
            'id': iteration.id,
            'date': iteration.created_date,
        }
        if iteration.status == str(IterationStatus.REVIEWED):
            reviewed_list.append(obj)
        else:
            not_reviewed_list.append(obj)
    context = {
        'iterations_reviewed': reviewed_list,
        'iterations_not_reviewed': not_reviewed_list,
        'session_id': session_id,
        'session_name': session.title,
    }
    return render(request, 'iterations_list.html', context=context)


# TODO: transactional
def iteration_view(request, iteration_id):
    iteration = get_object_or_404(Iteration, pk=iteration_id)

    if iteration.status == str(IterationStatus.NOT_STARTED):
        return HttpResponse("Still not started")
    if iteration.status == str(IterationStatus.IN_PROGRESS):
        return HttpResponse("Still in progress")
    if iteration.status == str(IterationStatus.REVIEWED):
        return HttpResponse("Already reviewed")

    groups_list_view = []

    for group in iteration.group_set.all():
        groups_list_view.append({
            'sentences': [
                {
                    'text': sent.text,
                    'preview': "count: " + str(sent.count) + "; important words: " + sent.important_words
                }
                for sent in list(group.sentence_set.order_by('-count').all())],
            'id': group.id
        })

    context = {
        'groups_list': groups_list_view,
        'session_id': iteration.session_id,
        'iteration_id': iteration.id,
        'input_text': iteration.input_value.important_words,
    }
    return render(request, 'iteration.html', context=context)


# TODO: transactional
def submit_iteration_review(request, session_id, iteration_id):
    group_ids_list: List[int] = []
    for field in request.POST:
        prefix = "group"
        field_text: str = field
        if field_text.startswith(prefix):
            group_id = int(field_text[len(prefix):len(field_text)])
            group_ids_list.append(group_id)

    session_service.review_iteration(session_id, iteration_id, group_ids_list)
    return HttpResponseRedirect("/session/" + str(session_id))


# TODO: transactional
def clear(request):
    session_service.clear_db()
    return HttpResponse('Done')
